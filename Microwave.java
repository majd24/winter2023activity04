public class Microwave {
	private String brand;
	private String color;
	private double price;
	
	// private String feature;
	
	/* public void displayBrand() {
		System.out.println(brand);	
	}
	public void reheatFood() {
		System.out.println("HEATING FOOD HAAA!");
	} */
	
	/* public void makePopcorn(String food) {
		if(food.equals("popcorn")) {
			// System.out.println("POP POP POPCORN!!!");
			this.feature = food;
			
		}	
	} */
	
	public void setBrand(String newBrand) {
		this.brand = newBrand;
		
	}
	public String getBrand() {
		
		return this.brand;
	}
	
	public void setColor(String newColor) {
		
		this.color = newColor;
		
	}
	
	public String getColor() {
		
		return this.color;
	}
	
	
	
	public void setPrice(Double newPrice) {
		
		this.price = newPrice;
	} 
	
	
	public double getPrice() {
		
		return this.price;
		
	}
	
	public Microwave(String brand, String color, double price) {
		this.brand = brand;
		this.color = color;
		this.price = price;
		
		
	}
}